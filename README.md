# cuicui

A social networks watcher for noobs!

## Installation

> This app can be launched in local host. 

### What you need
* [NodeJS](https://nodejs.org/)
* a web browser
* a valid Twitter account with dev features

### How to
1. `git clone`
2. `npm install`
3. Create an Twitter app at [apps.twitter.com](https://apps.twitter.com/app/new).
4. Fill up _/data/app/tokens.json_ file, with the data of your app

 ```
 {
    "consumer_key":"...",
    "consumer_secret":"...",
    "access_token":"...",
    "access_token_secret":"..."
 }
 ```
5. Fill up _/data/app/authAccounts.json_ and _/data/app/botAccounts.json_ with IDs of, in order, your account and your bots accounts (account used to stream data).
 
6. Launch the script

 ```
 node apps.js
 ```
 
7. Connect to your page web server on port :8888
8. That's it. So simple!


## Contribute

>Yay!

Just contact me at [@DavidLibeau](https://twitter.com/DavidLibeau) or [@David@Mastodon.xyz](https://mastodon.xyz/David)! 
