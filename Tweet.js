var mkdirp = require('mkdirp');
var fs = require('fs');
var getDirName = require('path').dirname;

function writeFile(path, contents, cb) {
    mkdirp(getDirName(path), function (err) {
        if (err) return cb(err);

        fs.writeFile(path, contents, cb);
    });
}


module.exports = class Tweet {
    constructor(data) {
        this.data = data;
    }
    exportForDb() {
        var exportedTweet = {
            created_at: this.data.created_at,
            id: this.data.id,
            text: this.data.text,
            source: this.data.source,
            author_id: this.data.user.id,
            author_username: this.data.user.username,
        };
        return (exportedTweet);
    }

    saveInDb() {

        /*fs.appendFile("./data/base/index.json", JSON.stringify(this.exportForDb()) + ",\r\n", function (err) {
            if (err) {
                return console.log("ERROR AppendIndex: " + err);
            }
        });*/

        var streamAccounts = require("./data/app/streamAccounts.json");

        if (streamAccounts.indexOf(this.data.user.id) != -1) {
            fs.readdir("./data/base/" + this.data.user.id + "/tweets/" + this.data.id, (err, files) => {
                if (files == undefined || files.length == 0) {
                    this.writeTweetInDb(this.data);
                } else {
                    var needToWrite = true;
                    for (var f in files) {
                        files[f].replace(".json", "");
                        var d = new Date();
                        if (parseInt(files[f]) + 60 * 60 * 1000 > d.valueOf()) {
                            needToWrite = false;
                        }
                    }
                    if (needToWrite) {
                        this.writeTweetInDb(this.data);
                    }
                }
            });
            this.saveUserInDb(this.data.user);
        }


        if (this.data.retweeted_status != undefined && streamAccounts.indexOf(this.data.retweeted_status.user.id) != -1) {
            fs.readdir("./data/base/" + this.data.retweeted_status.user.id + "/tweets/" + this.data.retweeted_status.id, (err, files) => {
                if (files == undefined || files.length == 0) {
                    this.writeTweetInDb(this.data.retweeted_status);
                } else {
                    var needToWrite = true;
                    for (var f in files) {
                        files[f].replace(".json", "");
                        var d = new Date();
                        if (parseInt(files[f]) + 60 * 60 * 1000 > d.valueOf()) {
                            needToWrite = false;
                        }
                    }
                    if (needToWrite) {
                        this.writeTweetInDb(this.data.retweeted_status);
                    }
                }
            });
            this.saveUserInDb(this.data.retweeted_status.user);
        }

        if (this.data.quoted_status != undefined && streamAccounts.indexOf(this.data.quoted_status.user.id) != -1) {
            fs.readdir("./data/base/" + this.data.quoted_status.user.id + "/tweets/" + this.data.quoted_status.id, (err, files) => {
                if (files == undefined || files.length == 0) {
                    this.writeTweetInDb(this.data.quoted_status);
                } else {
                    var needToWrite = true;
                    for (var f in files) {
                        files[f].replace(".json", "");
                        var d = new Date();
                        if (parseInt(files[f]) + 60 * 60 * 1000 > d.valueOf()) {
                            needToWrite = false;
                        }
                    }
                    if (needToWrite) {
                        this.writeTweetInDb(this.data.quoted_status);
                    }
                }

            });
            this.saveUserInDb(this.data.quoted_status.user);
        }
    }

    writeTweetInDb(tweetData) {
        var d = new Date();
        writeFile("./data/base/" + tweetData.user.id + "/tweets/" + tweetData.id + "/" + d.valueOf() + ".json", JSON.stringify(tweetData), function (err) {
            if (err) {
                return console.log("ERROR TweetWrite: " + err);
            }
        });
    }

    saveUserInDb(userData) {
        fs.readdir("./data/base/" + userData.id + "/user/", (err, files) => {
            if (files == undefined || files.length == 0) {
                this.writeUserInDb(userData);
            } else {
                var needToWrite = true;
                for (var f in files) {
                    files[f].replace(".json", "");
                    var d = new Date();
                    if (parseInt(files[f]) + 7 * 24 * 60 * 60 * 1000 > d.valueOf()) {
                        needToWrite = false;
                    }
                }
                if (needToWrite) {
                    this.writeUserInDb(userData);
                }
            }
        });
    }

    writeUserInDb(userData) {
        var d = new Date();
        writeFile("./data/base/" + userData.id + "/user/" + d.valueOf() + ".json", JSON.stringify(userData), function (err) {
            if (err) {
                return console.log("ERROR UserWrite: " + err);
            }
        });
    }
}

