var fs = require('fs');

module.exports = class Search {
    constructor(query) {
        this.query = query;
        this.equalFilters = [];
        this.text = "";
    }
    process(callback) {
        this.callback=callback;
        var queryArray = this.query.split(" ");
        for (var q in queryArray) {
            if (queryArray[q].indexOf(":") != -1) {
                var cmd = queryArray[q].split(":");
                var cmdName = cmd[0];
                var cmdQuery = cmd[1];
                if(cmd[1]=="" && cmd[2]!=undefined){
                    cmdQuery=":"+cmd[2];
                }
                switch (cmdName) {
                    case "to":
                    case "reply":
                        if (cmdQuery == "true") {
                            this.needReply = true;
                        } else if (cmdQuery == "false") {
                            this.needReply = false;
                        } else if (cmdQuery[0] == ":") {
                            this.equalFilters.push(["in_reply_to_user_id", cmdQuery.replace(":","")]);
                        } else {
                            this.equalFilters.push(["in_reply_to_screen_name", cmdQuery]);
                        }
                        break;
                    case "from":
                        if (cmdQuery[0] == ":") {
                            this.equalFilters.push(["author_id", cmdQuery.replace(":","")]);
                        } else {
                            this.equalFilters.push(["author_screen_name", cmdQuery]);
                        }
                        break;
                    case "rt":
                        if (cmdQuery == "true") {
                            this.needRT = true;
                        } else if (cmdQuery == "false") {
                            this.needRT = false;
                        } else if (cmdQuery[0] == ":") {
                            this.equalFilters.push(["retweeted_status_id", cmdQuery.replace(":","")]);
                        }
                        break;
                    case "rt_user":
                        if (cmdQuery[0] == ":") {
                            this.equalFilters.push(["retweeted_status_author_id", cmdQuery.replace(":","")]);
                        } else {
                            this.equalFilters.push(["retweeted_status_author_screen_name", cmdQuery]);
                        }
                        break;
                    case "quote":
                    case "cite":
                        if (cmdQuery == "true") {
                            this.needQuote = true;
                        } else if (cmdQuery == "false") {
                            this.needQuote = false;
                        } else if (cmdQuery[0] == ":") {
                            this.equalFilters.push(["quoted_status_id", cmdQuery.replace(":","")]);
                        }
                        break;
                    case "quote_user":
                    case "cite_user":
                        if (cmdQuery[0] == ":") {
                            this.equalFilters.push(["quoted_status_author_id", cmdQuery.replace(":","")]);
                        } else {
                            this.equalFilters.push(["quoted_status_author_screen_name", cmdQuery]);
                        }
                        break;
                    default:
                        this.equalFilters.push(cmdName, cmdQuery);
                        break;
                }
            } else {
                this.text += " " + queryArray[q];
            }
        }
        var search = this;
        fs.readFile("./data/base/index.json", 'utf8', function (err, data) {
            if (err) throw err;
            var database = JSON.parse("[" + data + "{}]");
            var regex = new RegExp(search.text, "gmi");
            var results = [];
            for (var d in database) {
                var matches=[];
                if (database[d].text != undefined) {
                    for(var f in search.equalFilters){
                        if(database[d][search.equalFilters[f][0]]!=undefined){
                            if(database[d][search.equalFilters[f][0]]==search.equalFilters[f][1]){
                                matches.push(true);
                            }else{
                                matches.push(false);
                            }
                        }
                    }
                    if(search.needReply!=undefined){
                        matches.push(search.needReply==(database[d].in_reply_to_user_id!=undefined));
                    }
                    if(search.needRT!=undefined){
                        matches.push(search.needRT==(database[d].retweeted_status_id!=undefined));
                    }
                    if(search.needQuote!=undefined){
                        matches.push(search.needQuote==(database[d].quoted_status_id!=undefined));
                    }
                    if (regex.test(database[d].text)) {
                        matches.push(true);
                    }else{
                        matches.push(false);
                    }
                }else{
                    matches.push(false);
                }
                var totalMatch=true;
                for(var m in matches){
                    if(matches[m]==false){
                        totalMatch=false;
                    }
                }
                if(totalMatch){
                    results.push(database[d]);
                }
            }
            search.callback(results);
        });
    }
}

/*
var exportedTweet = {
    created_at: this.data.created_at,
    id: this.data.id_str,
    text: [this.data.extended_tweet != undefined ? this.data.extended_tweet.full_text : this.data.text],
    source: this.data.source,
    author_id: this.data.user.id_str,
    author_screen_name: this.data.user.screen_name,
};
if (this.data.in_reply_to_status_id_str != null) {
    exportedTweet.in_reply_to_status_id = this.data.in_reply_to_status_id_str;
    exportedTweet.in_reply_to_user_id = this.data.in_reply_to_user_id_str;
    exportedTweet.in_reply_to_screen_name = this.data.in_reply_to_screen_name;
}
if (this.data.retweeted_status != undefined) {
    exportedTweet.retweeted_status_created_at = this.data.retweeted_status.created_at;
    exportedTweet.retweeted_status_id = this.data.retweeted_status.id_str;
    exportedTweet.retweeted_status_text = [this.data.retweeted_status.extended_tweet != undefined ? this.data.retweeted_status.extended_tweet.full_text : this.data.retweeted_status.text];
    exportedTweet.retweeted_status_author_id = this.data.retweeted_status.user.id_str;
    exportedTweet.retweeted_status_author_screen_name = this.data.retweeted_status.user.screen_name;
}
if (this.data.quoted_status != undefined) {
    exportedTweet.quoted_status_created_at = this.data.quoted_status.created_at;
    exportedTweet.quoted_status_id = this.data.quoted_status.id_str;
    exportedTweet.quoted_status_text = [this.data.quoted_status.extended_tweet != undefined ? this.data.quoted_status.extended_tweet.full_text : this.data.quoted_status.text];
    exportedTweet.quoted_status_author_id = this.data.quoted_status.user.id_str;
    exportedTweet.quoted_status_author_screen_name = this.data.quoted_status.user.screen_name;
}
*/