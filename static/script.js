/* 
* jquery.method getCursorPosition
* Get the text cursor position when the user is in an input
* 
*/
(function($) {
    $.fn.getCursorPosition = function() {
        var input = this.get(0);
        if (!input) return; // No (input) element found
        if ('selectionStart' in input) {
            // Standard-compliant browsers
            return input.selectionStart;
        } else if (document.selection) {
            // IE
            input.focus();
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
})(jQuery);


$('[data-toggle="tooltip"]').tooltip()


var tweetTemplate = $("#tweetTemplate").html();


var socket = io();
// Init
socket.on('init', function (data) {
    console.log("//Connected to server //Client id:["+data.id+"]");
    socket.emit('init', { data: 'ping' });
});
// NodeJS to client Console
socket.on('console', function (json) {
    console.log(json.data);
});




/* Omnisearch */

function addonRefresh(){
    $(".btn-addon").click(function(){
        $("#omnisearch .input-group-addon").remove();
        $('#omnisearch input[type="text"]').before('<span class="input-group-addon">'+$(this).data("addon")+'</span>');
    });
}
addonRefresh();

$('#omnisearch input[type="text"]').on("change",function(){
    //console.log($(this).val());
    if(/^inspect:\/\//i.test($(this).val())){
        $("#omnisearch .input-group-addon").remove();
        $('#omnisearch input[type="text"]').before('<span class="input-group-addon">inspect://</span>');
        $(this).val($(this).val().replace("inspect://",""));
    }else if(/^view:\/\//i.test($(this).val())){
        $("#omnisearch .input-group-addon").remove();
        $('#omnisearch input[type="text"]').before('<span class="input-group-addon">view://</span>');
        $(this).val($(this).val().replace("view://",""));
    }
});

$('#omnisearch input[type="text"]').keydown(function(e){
    //if($(this).val()=="" || $(this).val()==undefined){
    //console.log($(this).getCursorPosition());
    if($(this).getCursorPosition()==0){
        if(e.keyCode == 8 || e.keyCode == 46) {
            $("#omnisearch .input-group-addon").remove();
        }
    }
});

$("#omnisearch").on("submit",function(evt){
    evt.preventDefault();
    $("#omnisearchResults").html("");
    var search=$('#omnisearch input[type="text"]').val();
    if(search==undefined || search==""){ //Nothing is searched
        $("#omnisearchResults").html('<li class="alert alert-danger" role="alert"> <strong>Empty search</strong> Type something before searching.</li>');
    }
    if($("#omnisearch .input-group-addon").html()==undefined || $("#omnisearch .input-group-addon").html()==""){ //Without addon
        if(/^http(s?):\/\/twitter.com\//i.test(search)){ //A twitter.com link is searched
            $("#omnisearchResults").html('<li class="alert alert-warning" role="alert">Did you mean <strong class="btn-addon" data-addon="inspect://">inspect://'+search+'</strong> ?</li>');
            addonRefresh();
        }else{ // Regular search
            socket.emit('request', { 
                type: 'search',
                query: search
            });
        }
    }else if($("#omnisearch .input-group-addon").html()=="inspect://" || $("#omnisearch .input-group-addon").html()=="view://"){ //Addon inspect
        search=search.split("/");
        for(each in search){
            if($.isNumeric(search[each])){
                socket.emit('request', { 
                    type: 'tweet/lookup',
                    query: search[each]
                });
                console.log("tweet/lookup:"+search[each]);
                break;
            }
        }
    }
});

socket.on('respond', function (data) {
    if(data.type=='search'){
        console.log(data.response);
        if(data.response.statuses.length==0){
            $("#omnisearchResults").append('<li class="alert alert-info" role="alert"> <strong>No results :(</strong> Try to search something else.</li>');
        }else{
            for(tweet in data.response.statuses){
                renderTweet(data.response.statuses[tweet],"#omnisearchResults");
            }
        }
    }else if(data.type=='tweet/lookup'){
        console.log(data.response);
        renderTweet(data.response,"#omnisearchResults");
        if($("#omnisearch .input-group-addon").html()=="inspect://"){
            $("#omnisearchResults").append('<li><pre>'+JSON.stringify(data.response, null, ' ').replace(/</g,"&lt;").replace(/>/g,"&gt;")+'</pre></li>');
        }
    }
});



/* themeSelector */

$('#themeSelector input[type="checkbox"]').on("change",function(){
    if($(this).is(":checked")){
        $("head").append('<link href="'+$(this).val()+'" rel="stylesheet"/>'); 
    }else{
        $('link[href="'+$(this).val()+'"').remove();
    }
});
