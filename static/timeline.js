
socket.on('timelineUpdate', function (data) {
    //console.log(data);
    renderTweet(data.tweet,"#timeline>ul");

    //Manage scroll
    if($('[href="#timeline"]~.dropdown-menu .autoscroll>input').is(":checked")==false){
		var currentScroll=$("#timeline").scrollTop()
		//console.log("oldScroll: "+oldScroll+" + Tweet outerHeight: "+$("#tweet"+data.tweet.id_str).outerHeight(true));
        var timelineToScroll=$("#timeline").data("scroll")+$("#tweet"+data.tweet.id_str).outerHeight(true);
        $("#timeline").scrollTop(timelineToScroll);
		$("#timeline").data("scroll",currentScroll);
    }
  
});


/* 
* function renderTweet
* Render a tweet in a timaline via the itte.tw preprocessor
* 
* @params
* tweet : twitter.com api full object
* location : CSS selector of the timeline
*/
function renderTweet(tweet,location){
    var datapreprocess={ twitter:tweet, preprocess:preprocess(tweet)}
    console.log(datapreprocess);

    $(location).prepend(Mark.up(tweetTemplate, datapreprocess));
}


/* 
* function preprocess
* itte.tw (internal) tweet text preprocessor : replace entites with html content (t.co/links, @mention, #hashtag, media...) 
* 
* @params
* tweet : twitter.com api full object
*/
function preprocess(tweet){
    var preprocessdata={tweet:""};
    if(tweet.full_text!=undefined){
        preprocessdata.tweet=tweet.full_text;
    }else{
        preprocessdata.tweet=tweet.text;
    }

    var tweet_entities=tweet.entities;
    var tweet_extended_entities=tweet.extended_entities;
    if(tweet.extended_tweet!=undefined){ //Truncated tweet
        preprocessdata.tweet=tweet.extended_tweet.full_text;
        tweet_entities=tweet.extended_tweet.entities;
        tweet_extended_entities=tweet.extended_tweet.extended_entities;
    }

    preprocessdata.source=tweet.source.replace('rel="nofollow"','target="_blank"');

    if(tweet.retweeted_status!=undefined){ //It's a RT
        preprocessdata.retweeted_status=preprocess(tweet.retweeted_status);
        preprocessdata.retweeted_status.source=tweet.retweeted_status.source.replace('rel="nofollow"','target="_blank"');
    }
    if(tweet.quoted_status!=undefined){ //It's a quoted tweet
        preprocessdata.quoted_status=preprocess(tweet.quoted_status);
        preprocessdata.quoted_status.source=tweet.quoted_status.source.replace('rel="nofollow"','target="_blank"');
    }

    //console.log(util.inspect(tweet.entities.hashtags));
    for(var i in tweet_entities.hashtags){ //hashtags
        var hashtag=tweet_entities.hashtags[i];
        preprocessdata.tweet=preprocessdata.tweet.replace(new RegExp("[^>]#"+hashtag.text+"($|(<\/a>){0})","i"),Mark.up($("#hashtagTemplate").html(), hashtag));
    }
    for(var i in tweet_entities.urls){ //links
        var link=tweet_entities.urls[i];
        preprocessdata.tweet=preprocessdata.tweet.replace(link.url,Mark.up($("#linkTemplate").html(), link));
    }
    for(var i in tweet_entities.user_mentions){ //mention
        var mention=tweet_entities.user_mentions[i];
        preprocessdata.tweet=preprocessdata.tweet.replace(new RegExp("@"+mention.screen_name+"($|(<\/a>){0})","i"),Mark.up($("#mentionTemplate").html(), mention));
    }
    if(tweet_extended_entities){
        var medias="";
        for(var i in tweet_extended_entities.media){ //media
            var media=tweet_extended_entities.media[i];
            if(media.type=="photo"){
                medias+=Mark.up($("#imgTemplate").html(), media);
            }else if(media.type=="animated_gif" || media.type=="video"){
                medias+=Mark.up($("#videoTemplate").html(), media);
            }
        }
        preprocessdata.tweet=preprocessdata.tweet.replace(media.url,"<br/>"+medias);
    }
    preprocessdata.tweet=twemoji.parse(preprocessdata.tweet);
    //console.log(preprocessdata);


    /* Timestamp */		
    var date=new Date(tweet.created_at);
    preprocessdata.date=date.toLocaleString();


    return preprocessdata;
}


//Bootstrap
$(function () { //Bootstrap init
  $('[data-toggle="tooltip"]').tooltip();
});

$(".dropdown-menu>li>form").click(function(evt){ evt.stopPropagation() });

/* Timeline*/
$('.nav-tabs a').click(function(e) {
    e.preventDefault()
    var thisparent=$(this);
    setTimeout(function(){thisparent.tab('show')},100);
    setTimeout(function(){
        //$(thisparent.attr("href")).scrollTop($(thisparent.attr("href")).scrollTop()+1-1);
        console.log(thisparent.attr("href")+" "+$(thisparent.attr("href")).scrollTop());
		$(thisparent.attr("href")).data("scroll",0);
    },200);
    
    if($(this).attr("aria-expanded")=="true"){
        $($(this).attr("href")+".timeline").animate( { scrollTop: 0 }, 200 );
    }
    
    console.log($(this).attr("href")+".timeline>ul : "+$($(this).attr("href")+".timeline>ul").length+" && "+$(this).attr("href")+".timeline : "+$($(this).attr("href")+".timeline").scrollTop());
    if($($(this).attr("href")+".timeline>ul").length==1 && $($(this).attr("href")+".timeline").scrollTop()!=0){ // There is tweet(s) and the timeline is not scrolled to top 
        console.log("shadow on");
        $(".nav-tabs").css({
            "-webkit-box-shadow": "0 5px 10px -5px rgba(0,0,0,0.5)",
            "box-shadow": "0 5px 10px -5px rgba(0,0,0,0.5)"
        });
    }else{
        console.log("shadow off");
        $(".nav-tabs").css({
            "-webkit-box-shadow": "",
            "box-shadow": ""
        });
    }
});

$(".timeline").scroll(function(){
    //console.log($(this).scrollTop());
    if($(this).hasClass("active")){
        if($(this).scrollTop()!=0){
            $(".nav-tabs").css({
                "-webkit-box-shadow": "0 5px 10px -5px rgba(0,0,0,0.5)",
                "box-shadow": "0 5px 10px -5px rgba(0,0,0,0.5)"
            });
        }else{
            $(".nav-tabs").css({
                "-webkit-box-shadow": "",
                "box-shadow": ""
            });
        }
    }
	$(this).data("scroll",$(this).scrollTop());
});

