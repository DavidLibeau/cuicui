var tokens = require("./data/app/tokens.json");

const util = require("util");
var fs = require("fs");

var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io").listen(server);

var passport = require("passport");
var Strategy = require("passport-twitter").Strategy;
var connection = require("connect-ensure-login");

var Twitter11 = require('twit');
var Twitter2 = require('twitter-v2');

var Tweet = require("./Tweet.js");
var Search = require("./Search.js");

var botAccounts = require("./data/app/botAccounts.json");

var twitterTokens = [];
var T = [];

async function checkBotCredentials() {
    for (var b in botAccounts) {
        if (fs.existsSync("./data/app/" + botAccounts[b] + "_accessToken.json")) {
            var accessToken = require("./data/app/" + botAccounts[b] + "_accessToken.json");
            twitterTokens.push({
                token: accessToken.token,
                tokenSecret: accessToken.tokenSecret
            });
        }
    }
    await authBots(twitterTokens, 0);
}

async function authBots(twitterTokens, i) {
    return new Promise(async (resolve, reject) => {
        T.push({
            "v1_1": new Twitter11({
                consumer_key: tokens.consumer_key,
                consumer_secret: tokens.consumer_secret,
                access_token: twitterTokens[i].token,
                access_token_secret: twitterTokens[i].tokenSecret
            })
        });
        T[i].v1_1._getBearerToken(async function (error, bearerToken) {
            T[i].v2 = new Twitter2({
                bearer_token: bearerToken
            });
            i++;
            if (T[i] != undefined) {
                await authBots(twitterTokens, i);
            } else {
                console.log(T);
                return resolve(true);
            }
        })
    })
}

async function updateStreamAccounts() {
    if (T.length != 0) {
        if (fs.existsSync("./data/app/streamAccounts.json")) {
            var streamAccounts = require("./data/app/streamAccounts.json");
            //console.log(streamAccounts);
        } else {
            var streamAccounts = {};
        }
        for (var b in botAccounts) {
            T[0].v1_1.get("friends/list", {
                user_id: botAccounts[b],
                count: 200
            }, function (err, data, response) {
                //console.log(data);
                var following = data.users;
                for (var d in following) {
                    if (streamAccounts[following[d].screen_name] == undefined) {
                        streamAccounts[following[d].screen_name] = {
                            "id": following[d].id_str,
                            "screen_name": following[d].screen_name,
                            "group_id": "NaN"
                        };
                    }
                }
                console.log(streamAccounts);
                fs.writeFile("./data/app/streamAccounts.json", JSON.stringify(streamAccounts), function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });

                //rules

                var streamAccountsPerGroup = {};
                for (var a in streamAccounts) {
                    if (streamAccountsPerGroup[streamAccounts[a].group_id] == undefined) {
                        streamAccountsPerGroup[streamAccounts[a].group_id] = [];
                    }
                    streamAccountsPerGroup[streamAccounts[a].group_id].push(streamAccounts[a])
                }
                //console.log(streamAccountsPerGroup);
                var rules = {};
                for (var g in streamAccountsPerGroup) {
                    var rule = "";
                    for (var a in streamAccountsPerGroup[g]) {
                        if (a > 0) {
                            rule += " OR ";
                        }
                        rule += "from:{user_name} OR to:{user_name} OR retweets_of:{user_name} OR {user_name}".replace(/{user_name}/g, streamAccountsPerGroup[g][a].screen_name);
                    }
                    rules[g] = rule;
                }
                //console.log(rules);
                T[0].v2.get('tweets/search/stream/rules')
                    .then(results => {
                        var onlineRules = results.data;
                        var missingRules = (rules ? JSON.parse(JSON.stringify(rules)) : []);
                        var extraRules = {};
                        var command = {
                            "add": [],
                            "delete": {
                                "ids": []
                            }
                        };
                        for (var r in onlineRules) {
                            extraRules[onlineRules[r].tag] = onlineRules[r];
                            if (rules[onlineRules[r].tag]) {
                                delete missingRules[onlineRules[r].tag]; //remove the rule from missingRules if the rule is already online
                                delete extraRules[onlineRules[r].tag]; //remove the rule from extraRules if the rule is already online
                                if (onlineRules[r].value === rules[onlineRules[r].tag]) {
                                    console.log("same rule " + onlineRules[r].id);
                                } else {
                                    console.log("different rule");
                                    command.delete.ids.push(onlineRules[r].id);
                                    command.add.push({
                                        "value": rules[onlineRules[r].tag],
                                        "tag": onlineRules[r].tag
                                    });
                                }
                            }
                        }
                        console.log("extra", extraRules);
                        console.log("missing", missingRules);
                        for (var r in extraRules) {
                            command.delete.ids.push(extraRules[r].id);
                        }
                        for (var r in missingRules) {
                            command.add.push({
                                "value": missingRules[r],
                                "tag": r
                            });
                        }
                        var commandScore = 0;
                        if (command.delete.ids.length == 0) {
                            commandScore++;
                            delete command.delete;
                        }
                        if (command.add.length == 0) {
                            commandScore++;
                            delete command.add;
                        }
                        console.log("command", command);
                        if (commandScore != 2) {
                            T[0].v2.post('tweets/search/stream/rules', command)
                                .then(results => {
                                    console.log(results);
                                }).catch(error => {
                                    console.log(error)
                                });
                        }
                    })
                    .catch(error => {
                        console.log(error)
                    });
            });
        }
    }
}

async function main() {
    await checkBotCredentials();
    await updateStreamAccounts();

    if (T.length == 0) {
        console.log("Error : no bot tokens in database.")
    } else {
        async function listenForever(streamFactory, dataConsumer) {
            try {
                for await (const { data } of streamFactory()) {
                    dataConsumer(data);
                }
                console.log('Stream disconnected healthily. Reconnecting.');
                listenForever(streamFactory, dataConsumer);
            } catch (error) {
                console.warn('Stream disconnected with error. Retrying in 5 secondes…', error);
                setTimeout(function(){
                    listenForever(streamFactory, dataConsumer);
                }, 5000);
            }
        }

        listenForever(
            () => T[0].v2.stream('tweets/search/stream', {
                expansions: ['author_id', 'entities.mentions.username', 'referenced_tweets.id', 'referenced_tweets.id.author_id'],
                tweet: {
                    fields: [ 'id', 'text', 'created_at', 'public_metrics', 'source', 'referenced_tweets', 'author_id']
                },
                user: {
                    fields: [ 'id', 'username', 'created_at', 'public_metrics', 'verified']
                }
            }),
            (response) => {
                console.log("tweet", response);
                tw = new Tweet(response);
                tw.saveInDb();
                //console.log(tw.exportForDb());
                //console.log(tw.data);
                io.emit("timelineUpdate", {
                    tweet: tw.data
                });
            }
        );
    }
}
main();

var trustProxy = false;
if (process.env.DYNO) {
    // Apps on heroku are behind a trusted proxy
    trustProxy = true;
}


// Configure the Twitter strategy for use by Passport.
//
// OAuth 1.0-based strategies require a `verify` function which receives the
// credentials (`token` and `tokenSecret`) for accessing the Twitter API on the
// user"s behalf, along with the user"s profile.  The function must invoke `cb`
// with a user object, which will be set at `req.user` in route handlers after
// authentication.
passport.use(new Strategy({
        consumerKey: tokens.consumer_key,
        consumerSecret: tokens.consumer_secret,
        callbackURL: "http://127.0.0.1:8888/oauth/callback",
        proxy: trustProxy
    },
    function (token, tokenSecret, profile, cb) {
        var botAccounts = require("./data/app/botAccounts.json");
        if (botAccounts.indexOf(profile.id) != 1) {
            console.log()
            fs.writeFile("./data/app/" + profile.id + "_accessToken.json", JSON.stringify({
                token: token,
                tokenSecret: tokenSecret
            }), function (err) {
                if (err) {
                    return console.log(err);
                }
                checkBotCredentials();
            });
        }
        console.log("token:" + token)
        console.log("tokenSecret:" + tokenSecret);
        console.log("profile:");
        console.log(profile);
        // In this example, the user"s Twitter profile is supplied as the user
        // record.  In a production-quality application, the Twitter profile should
        // be associated with a user record in the application"s database, which
        // allows for account linking and authentication with other identity
        // providers.
        return cb(null, profile);
    }));


// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  In a
// production-quality application, this would typically be as simple as
// supplying the user ID when serializing, and querying the user record by ID
// from the database when deserializing.  However, due to the fact that this
// example does not have a database, the complete Twitter profile is serialized
// and deserialized.
passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});


// Configure view engine to render EJS templates.
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require("morgan")("combined"));
app.use(require("cookie-parser")());
app.use(require("body-parser").urlencoded({
    extended: true
}));
app.use(require("express-session")({
    secret: "cuicuicui",
    resave: true,
    saveUninitialized: true
}));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());



// Define routes.
app.get("/admin",
    function (req, res) {
        if (req.user != undefined) {
            var authAccounts = require("./data/app/authAccounts.json");
            if (authAccounts.indexOf(req.user.id) != -1) {
                res.render("admin", {
                    user: req.user
                });
            } else {
                res.render("unauthprofile", {
                    user: req.user
                });
            }
        } else {
            res.render("admin", {
                user: false
            });
        }
    });

app.get("/admin/login",
    function (req, res) {
        console.log("ENV");
        console.log(process.env);
        console.log("Headers:");
        console.log(req.headers)
        res.render("login");
    });

app.get("/admin/login/twitter",
    passport.authenticate("twitter"));

app.get("/oauth/callback",
    passport.authenticate("twitter", {
        failureRedirect: "/admin/login"
    }),
    function (req, res) {
        res.redirect("/admin");
    });

app.get("/admin/profile",
    connection.ensureLoggedIn("/admin/login"),
    function (req, res) {
        var authAccounts = require("./data/app/authAccounts.json");
        if (authAccounts.indexOf(req.user.id) != -1) {
            res.render("profile", {
                user: req.user
            });
        } else {
            res.render("unauthprofile", {
                user: req.user
            });
        }
    });

app.get("/admin/logout", function (req, res) {
    req.logout();
    res.redirect("/");
});

app.get("/admin/profile/timeline",
    connection.ensureLoggedIn("/admin/login"),
    function (req, res) {
        var authAccounts = require("./data/app/authAccounts.json");
        if (authAccounts.indexOf(req.user.id) != -1) {
            res.render("profile", {
                user: req.user
            });
        } else {
            res.render("unauthprofile", {
                user: req.user
            });
        }
    });


server.listen(process.env["PORT"] || 8888, function () {
    console.log("SERVER listening on *:8888");
});



app.get("/", function (req, res) {
    var authAccounts = require("./data/app/authAccounts.json");
    if (req.user != undefined && authAccounts.indexOf(req.user.id) != -1) {
        res.render("index", {
            admin: true,
            user: req.user
        });
    } else {
        res.render("index", {
            admin: false,
            user: req.user
        });
    }
});

app.use(express.static("static"));

var id = 0;

io.on("connection", function (socket) {
    var userid = id++;
    socket.emit("init", {
        id: userid
    });
    socket.on("init", function (data) {
        console.log("//Client [" + userid + "] connected");
    });

    socket.on("disconnect", function () {
        //stream.stop();
        console.log("//Client [" + userid + "] disconnected");
    });


    // Search / Inspect

    socket.on("request", function (data) {
        var query = new Search(data.query);
        query.process(function (results) {
            console.log(results);
        });
        /*if (data.type == "search") {
            T[0].get("search/tweets", {
                q: data.query,
                count: 10
            }, function (err, data, response) {
                socket.emit("respond", {
                    type: "search",
                    response: data
                });
            });
        } else if (data.type == "tweet/lookup") {
            T[0].get("statuses/show", {
                id: data.query
            }, function (err, data, response) {
                socket.emit("respond", {
                    type: "tweet/lookup",
                    response: data
                });
            });
        }*/
    });


});