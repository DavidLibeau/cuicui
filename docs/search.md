# Search docs

## Search query

Any text will be searched in the tweet content.

## Filters

### WIP `created_at`:
- Type: exact match
- Content:
    - *string* date
    
### `id:`
- Type: exact match
- Content:
    - *int* id
    
### WIP `source:`
- Type: exact match
- Content:
    - *int* id
    
### `author_id:`
- Type: exact match
- Content:
    - *int* id
    
### `author_screen_name:`
- Type: exact match
- Content:
    - *int* id
    
### `in_reply_to_status_id:`
- Type: exact match
- Content:
    - *int* id
    
### `in_reply_to_user_id:` / `reply::` / `to::`
- Type: exact match
- Content:
    - *int* id
    
### `in_reply_to_screen_name:` / `reply:` / `to:`
- Type: exact match
- Content:
    - *string* username
            
### WIP `retweeted_status_created_at:`
- Type: exact match
- Content:
    - *string* date
    
### `rt:`
- Type: test
- Content:
    - *boolean* withRT
    
### `retweeted_status_id:` / `rt::`
- Type: exact match
- Content:
    - *int* id
    
### `retweeted_status_author_id:` / `rt_user::`
- Type: exact match
- Content:
    - *int* id
    
### `retweeted_status_author_screen_name:` / `rt_user:`
- Type: exact match
- Content:
    - *string* username

### WIP `quoted_status_created_at:`
- Type: exact match
- Content:
    - *string* date
    
### `quote:` / `cite:`
- Type: test
- Content:
    - *boolean* withQuote

### `quoted_status_id:` / `quote::` / `cite::`
- Type: exact match
- Content:
    - *int* id
    
### `quoted_status_author_id:` / `quote_user::` / `cite_user::`
- Type: exact match
- Content:
    - *int* id
    
### `quoted_status_author_screen_name:` / `quote_user:` / `cite_user:`
- Type: exact match
- Content:
    - *string* username
    