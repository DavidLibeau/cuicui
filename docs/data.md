# Data

## Message data structure

File name: `{plateform_id}_{id}.json`

```
{
    "plateform_id": "twitter",
    "content": "This is the text of the tweet!",
    "created_at": "ISO_datetime",
    "groupe_id": "macron|melenchon|…",
    "share_count": 0,
    "comment_count": 0
}
```

## Interaction data structure

File name: `{type}_{sha256({plateform_id}{id}{salt})}.json`

```
{
    "type": "share|comment",
    "created_at": "ISO_datetime",
    "groupe_id": "macron|melenchon|…",
    "parent_id": {id},
    "local": {
        "type": "retweet|like|reply|citation|dislike|…",
        "is_from_monitored_community": "true|false|undefined",
        "followers": 0,
        "following": 0,
        "source_tool": "Twiter.com",
        "user_created_at": ""
    }
}
```